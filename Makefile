TARGET = iphone:clang:latest:13.0
INSTALL_TARGET_PROCESSES = ValkyrieJB
ARCHS = arm64 arm64e

include $(THEOS)/makefiles/common.mk

APPLICATION_NAME = ValkyrieJB

ValkyrieJB_FILES = AppDelegate.swift RootViewController.swift
ValkyrieJB_FRAMEWORKS = UIKit CoreGraphics

include $(THEOS_MAKE_PATH)/application.mk
